Send commands to Taiga.io from hubot

# Features

- Should work with any chat solution. Tested in Let's Chat and Slack.
- Make changes to Taiga User Stories (more coming soon) with chat commands. 
- Same syntax as Taiga [git commit messages.](https://taiga.io/support/changing-elements-status-via-commit-message/)

![Screenshot](demo.png)

# Not yet features

- Create new tasks, ect
- Change assigned user or other attributes
- Your great merge request you are going to send very soon (maybe raise an issue first with your idea)

# Install

1. Make sure hubot is set up and redis is working (optional, but suggested)
2. Install from npm like `npm install hubot-taiga`
3. Add `'hubot-taiga'` to your hubot's `external-scripts.json` file.
4. Set username/password as environment variables. I use a special Hubot user made in Taiga.io. User should have admin privledges.
`HUBOT_TAIGA_USERNAME` and `HUBOT_TAIGA_PASSWORD` and optional `HUBOT_TAIGA_PROJECT`
This step is technically optional but it makes it easier for chat users to use hubot-taiga
5. Restart hubot

# Usage

Commands

- `taiga info` - view hubot-taiga settings
- `taiga project <project slug>` - Set project per channel
- `taiga auth <username> <password>` - Log in to post comments as your Taiga.io user
- `TG-REF Your comment` Add a comment where REF is the User Story reference.
Example `TG-123 I think this is a great idea`
and `TG-123 #done I finished it, I am the best.`

Hubot taiga can use a globally set (from your environment variables) user to post. 
This makes it easier for chat users to just do things without logging in.
You may also login with the `taiga auth` command. This will make hubot post your user taiga user. 
Your auth token will be stored in redis - however your password will not be.
**Except** it will be in your chat history potentially.

Note you may set the Taiga Project slug you wish to work with both globally and/or per chat channel with `taiga project`

# Developing

There are many ways to work on hubot scripts. Here is how I do it.

## Docker

This method uses docker-compose

1. Clone this repo
2. Run `docker-compose run --rm hubot bash`
3. Run inside the container `bin/hubot`

## Without Docker

1. Clone this repo
2. Install a new hubot just for testing.
3. `cd` to your new hubot install
4. Set environment variables (see INSTALLATION). I write a small script for this.
5. Run hubot like `bin/hubot`
6. Test out your command. You will need to restart hubot for each change.
7. Submit a merge request with better instructions (Auto-restart? Debugging?)

## Unit Testing

I don't have this dockerized at this time. Install all dev dependencies and run tests with `gulp mocha`.
See the (spec)[/spec] folder for unit tests.

Contributions should include a unit test in order to be accepted. 
Test flow should send a hubot command and then inspect the mocked response replies to ensure it's what is expected.
You should be familair with the mocha and nock packages first.
