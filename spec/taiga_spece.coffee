path = require 'path'
Robot = require("hubot/src/robot")
TextMessage = require("hubot/src/message").TextMessage
chai = require 'chai'
nock = require 'nock'
{ expect } = chai


describe 'taiga', ->
  robot = null
  user = null
  adapter = null
  nockScope = null
  username = 'user'
  password = 'pass'
  authToken = 'fake token'
  projectSlug = 'bufke-hubot-taiga'
  beforeEach (done)->
    process.env.HUBOT_TAIGA_USERNAME = username
    process.env.HUBOT_TAIGA_PASSWORD = password

    nock.disableNetConnect()
    nockScope = nock('https://api.taiga.io/api/v1')
    robot = new Robot null, 'mock-adapter', yes, 'TestHubot'
    robot.adapter.on 'connected', ->
      robot.loadFile path.resolve('.', 'src'), 'taiga.coffee'
      user = robot.brain.userForId '1', {
        name: 'ngs'
        room: '#mocha'
      }
      adapter = robot.adapter
      do done
    do robot.run
  it 'should get and set taiga project', (done)->
    adapter.on 'send', (envelope, strings)->
      try
        expect(strings[0]).to.contain '#mocha'
        expect(strings[0]).to.contain projectSlug
        do done
      catch e
        done e
    adapter.receive new TextMessage user, 'taiga project bufke-hubot-taiga'

  it 'should send comments', (done)->
    scope = nockScope.post('/auth', {
      type: "normal",
      username: username,
      password: password
    })
      .reply 200, {
        auth_token: authToken
      }

    url = '/resolver?project=' + projectSlug + '&us=1'
    scope = nockScope.get(url)
      .reply 200, {
        us: 1,
        project: 1
      }

    url = '/userstories/1'
    scope = nockScope.get(url)
      .reply 200, {
        version: 1
      }

    url = '/userstory-statuses?project=1'
    scope = nockScope.get(url)
      .reply 200, [
        {
          id: 1,
          name: 'Nope',
          slug: 'nope'
        },
        {
          id: 2,
          name: 'Status',
          slug: 'status'
        }
      ]

    scope = nockScope.patch('/userstories/1', {
      comment: "ngs: #status comment",
      version: 1,
      status: 2
    })
      .reply 200, (uri, body) ->
        do done

    adapter.receive new TextMessage user, 'TG-1 #status comment'
